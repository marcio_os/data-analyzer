package com.ilegra.dataanalyzer.domain;

import java.util.HashMap;
import java.util.Map;

import com.ilegra.dataanalyzer.domain.customer.Customer;
import com.ilegra.dataanalyzer.domain.sale.Sales;
import com.ilegra.dataanalyzer.domain.salesman.Salesman;

/**
 * Enumerates the application main entities {@link Customer}, {@link Salesman} and {@link Sales}.
 * 
 * @author marcio
 */
public enum EntityType {

    SALESMAN(1),
    CUSTOMER(2),
    SALES(3);
    
    private static Map<Integer, EntityType> typeMap;
    
    private final int id;

    private EntityType(int id) {
        this.id = id;
    }
    
    public static EntityType valueOf(int id) {
        return getTypeMap().get(Integer.valueOf(id));
    }
    
    private static Map<Integer, EntityType> getTypeMap() {
        if (typeMap == null) {
            createTypeMap();
        }
        return typeMap;
    }

    private static void createTypeMap() {
        typeMap = new HashMap<Integer, EntityType>();
        for (EntityType type : values()) {
            typeMap.put(Integer.valueOf(type.id), type);
        }
    }
}
