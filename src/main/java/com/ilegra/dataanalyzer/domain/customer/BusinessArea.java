package com.ilegra.dataanalyzer.domain.customer;

import com.google.common.base.Objects;

/**
 * A business area.
 * 
 * @author marcio
 */
public class BusinessArea {

    private final String name;
    
    public BusinessArea(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(name);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BusinessArea) {
            BusinessArea that = (BusinessArea) obj;
            return Objects.equal(this.name, that.getName());
        }
        return false;
    }
}
