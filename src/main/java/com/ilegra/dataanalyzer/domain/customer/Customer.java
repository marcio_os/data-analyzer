package com.ilegra.dataanalyzer.domain.customer;

import com.google.common.base.Objects;

/**
 * A customer.
 * 
 * @author marcio
 */
public class Customer {

    private final Cnpj cnpj;
    private final String name;
    private final BusinessArea businessArea;

    public Customer(Cnpj cnpj, String name, BusinessArea businessArea) {
        this.cnpj = cnpj;
        this.name = name;
        this.businessArea = businessArea;
    }

    public Cnpj getCnpj() {
        return cnpj;
    }

    public String getName() {
        return name;
    }

    public BusinessArea getBusinessArea() {
        return businessArea;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(cnpj, name, businessArea);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Customer) {
            Customer that = (Customer) obj;
            return Objects.equal(this.cnpj, that.getCnpj()) && Objects.equal(this.name, that.getName())
                    && Objects.equal(this.businessArea, that.getBusinessArea());
        }
        return false;
    }
}
