package com.ilegra.dataanalyzer.domain.customer;

import javax.validation.constraints.Size;

import com.google.common.base.Objects;

/**
 * A taxpayer identification number.
 * 
 * @author marcio
 */
public class Cnpj {

    @Size(min = 14, max = 14)
    private final String number;
    
    public Cnpj(String number) {
        this.number = number;
    }
    
    public String getNumber() {
        return number;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(number);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Cnpj) {
            Cnpj that = (Cnpj) obj;
            return Objects.equal(this.number, that.getNumber());
        }
        return false;
    }
}
