package com.ilegra.dataanalyzer.domain.salesman;

import java.math.BigDecimal;

import com.google.common.base.Objects;

/**
 * A company salesman.
 * 
 * @author marcio
 */
public class Salesman {

    private final Cpf cpf;
    private final String name;
    private final BigDecimal salary;

    public Salesman(Cpf cpf, String name, BigDecimal salary) {
        this.cpf = cpf;
        this.name = name;
        this.salary = salary;
    }

    public Cpf getCpf() {
        return cpf;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(cpf, name, salary);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Salesman) {
            Salesman that = (Salesman) obj;
            return this.cpf.equals(that.getCpf())
                    && this.name.equals(that.getName())
                    && this.salary.equals(that.getSalary());
        }
        return false;
    }
}
