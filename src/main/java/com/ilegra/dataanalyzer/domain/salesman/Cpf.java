package com.ilegra.dataanalyzer.domain.salesman;

import javax.validation.constraints.Size;

import com.google.common.base.Objects;

/**
 * Brazilian social security number.
 * 
 * @author marcio
 */
public class Cpf {

    @Size(min = 11, max = 11)
    private final String number;

    public Cpf(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(number);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Cpf) {
            Cpf that = (Cpf) obj;
            return Objects.equal(this.number, that.getNumber());
        }
        return false;
    }
}
