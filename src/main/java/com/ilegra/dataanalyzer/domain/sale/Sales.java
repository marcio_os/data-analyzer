package com.ilegra.dataanalyzer.domain.sale;

import java.math.BigDecimal;
import java.util.Collection;

import com.google.common.base.Objects;
import com.ilegra.dataanalyzer.domain.salesman.Salesman;

/**
 * A series of {@link Sale sales} made by a {@link Salesman}.
 *
 * @author marcio
 */
public class Sales {

    private final int id;
    private final Collection<Sale> sales;
    private final String salesmanName;

    public Sales(int id, Collection<Sale> sales, String salesmanName) {
        this.id = id;
        this.sales = sales;
        this.salesmanName = salesmanName;
    }

    public int getId() {
        return id;
    }

    public Collection<Sale> getSales() {
        return sales;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public BigDecimal calculateTotalPrice() {
        BigDecimal totalPrice = new BigDecimal(0);

        for (Sale sale : sales) {
            totalPrice = totalPrice.add(sale.getItem().getPrice().multiply(BigDecimal.valueOf(sale.getNumberOfItems())));
        }
        return totalPrice;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, sales, salesmanName);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Sales) {
            Sales that = (Sales) obj;
            return Objects.equal(this.id, that.getId())
                    && Objects.equal(this.sales, that.getSales())
                    && Objects.equal(this.salesmanName, that.getSalesmanName());
        }
        return false;
    }
}
