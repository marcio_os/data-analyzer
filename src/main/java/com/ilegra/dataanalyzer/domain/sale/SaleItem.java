package com.ilegra.dataanalyzer.domain.sale;

import java.math.BigDecimal;

import com.google.common.base.Objects;
import com.ilegra.dataanalyzer.domain.salesman.Salesman;

/**
 * An item sold by a {@link Salesman}.
 * 
 * @author marcio
 */
public class SaleItem {

    private final int id;
    private final BigDecimal price;
    
    public SaleItem(int id, BigDecimal price) {
        this.id = id;
        this.price = price;
    }
    
    public int getId() {
        return id;
    }
    
    public BigDecimal getPrice() {
        return price;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(id, price);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SaleItem) {
            SaleItem that = (SaleItem) obj;
            return Objects.equal(this.id, that.getId())
                    && Objects.equal(this.price, that.getPrice());
        }
        return false;
    }
}
