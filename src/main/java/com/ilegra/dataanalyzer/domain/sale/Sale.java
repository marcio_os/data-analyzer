package com.ilegra.dataanalyzer.domain.sale;

import com.google.common.base.Objects;

/**
 * A sale of one or more {@link SaleItem}.
 * 
 * @author marcio
 */
public class Sale {

    private final SaleItem item;
    private final int numberOfItems;

    public Sale(SaleItem item, int numberOfItems) {
        this.item = item;
        this.numberOfItems = numberOfItems;
    }
    
    public SaleItem getItem() {
        return item;
    }
    
    public int getNumberOfItems() {
        return numberOfItems;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(item, numberOfItems);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Sale) {
            Sale that = (Sale) obj;
            return Objects.equal(this.item, that.getItem())
                    && Objects.equal(this.numberOfItems, that.getNumberOfItems());
        }
        return false;
    }
}
