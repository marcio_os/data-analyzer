package com.ilegra.dataanalyzer.analysis;

import org.springframework.stereotype.Component;

import com.ilegra.dataanalyzer.domain.EntityType;

/**
 * Discover what is the {@link EntityType} referenced by a given string.
 *
 * @author marcio
 */
@Component
public class EntityTypeResolver {

    /**
     * Returns discovered {@link EntityType} based on given string.
     *
     * @throws UnknownEntityException
     *             When {@link EntityTypeResolver} was unable to determine the {@link EntityType}.
     */
    public EntityType resolve(String string) throws UnknownEntityException {
        try {
            EntityType type = EntityType.valueOf(Integer.parseInt(string));

            if (type == null) {
                throw new UnknownEntityException("Could not determine entity type"); //$NON-NLS-1$
            }
            return type;
        } catch (NumberFormatException e) {
            throw new UnknownEntityException(e);
        }
    }
}
