package com.ilegra.dataanalyzer.analysis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.inject.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.common.io.LineProcessor;
import com.ilegra.dataanalyzer.analysis.parsing.SalesmanLineParser;
import com.ilegra.dataanalyzer.domain.EntityType;
import com.ilegra.dataanalyzer.domain.sale.Sale;
import com.ilegra.dataanalyzer.domain.sale.SaleItem;
import com.ilegra.dataanalyzer.domain.sale.Sales;

/**
 * Analyze data files and outputs calculated results.
 *
 * @author marcio
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FileAnalyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileAnalyzer.class);

    private final Provider<FileLineProcessor> fileLineProcessorProvider;
    private final String outputDirectory;

    @Inject
    public FileAnalyzer(Provider<FileLineProcessor> fileLineProcessorProvider, @Value("${outputDirectory}") String outputDirectory) {
        this.fileLineProcessorProvider = fileLineProcessorProvider;
        this.outputDirectory = outputDirectory;
    }

    public void analyze(File file) throws FileAnalysisException {
        BufferedWriter writer = null;

        LOGGER.info("Analyzing file {}", file.getName()); //$NON-NLS-1$
        try {
            String output = Files.readLines(file, Charsets.UTF_8, fileLineProcessorProvider.get());

            writer = createWriter(file);
            writer.write(output);
        } catch (IOException e) {
            throw new FileAnalysisException(e);
        } finally {
            closeWriter(writer);
        }
    }

    private BufferedWriter createWriter(File file) throws FileNotFoundException {
        String outputFile = outputDirectory + File.separator + (file.getName().split("\\.dat")[0]) + ".done.dat"; //$NON-NLS-1$//$NON-NLS-2$
        LOGGER.info("Writing file {}", outputFile); //$NON-NLS-1$
        return Files.newWriter(new File(outputFile), Charsets.UTF_8);
    }

    private void closeWriter(BufferedWriter writer) {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                LOGGER.error("Error while closing file writer"); //$NON-NLS-1$
            }
        }
    }

    @Component
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public static class FileLineProcessor implements LineProcessor<String> {

        private static final String LINE_ELEMENT_SEPARATOR = "�"; //$NON-NLS-1$

        private final EntityTypeResolver entityTypeResolver;
        private final SalesmanLineParser salesmanLineParser;

        private int amountOfClients = 0;
        private int amountOfSalesman = 0;
        private int mostExpensiveSaleId = -1;
        private BigDecimal mostExpensiveSalePrice = new BigDecimal(0);
        private Map<String, BigDecimal> salesmanTotalSalesPrice = new LinkedHashMap<String, BigDecimal>();
        private Map<String, BigDecimal> salesmanSalaries = new LinkedHashMap<String, BigDecimal>();

        @Inject
        public FileLineProcessor(EntityTypeResolver entityTypeResolver, SalesmanLineParser salesmanLineParser) {
            this.entityTypeResolver = entityTypeResolver;
            this.salesmanLineParser = salesmanLineParser;
        }

        @Override
        public boolean processLine(String line) throws IOException {
            String[] splittedLine = line.split(LINE_ELEMENT_SEPARATOR);

            if (splittedLine != null && splittedLine.length > 0) {
                try {
                    EntityType type = entityTypeResolver.resolve(splittedLine[0]);

                    if (type == EntityType.CUSTOMER) {
                        processCustomerLine();
                    } else if (type == EntityType.SALESMAN) {
                        processSalesmanLine(splittedLine);
                    } else {
                        int saleId = Integer.parseInt(splittedLine[1]);
                        Sales salesData = createSalesData(splittedLine, saleId);
                        updateMostExpensiveSalePrice(saleId, salesData);
                        updateSalesmanTotalSalesPrice(salesData);
                    }
                } catch (UnknownEntityException e) {
                    LOGGER.error("Could not determine entity type of line {}", line); //$NON-NLS-1$
                }
            }
            return true;
        }

        private void processCustomerLine() {
            amountOfClients++;
        }

        private void processSalesmanLine(String[] splittedLine) {
            amountOfSalesman++;
            String salesmanName = salesmanLineParser.extractName(splittedLine);
            BigDecimal salesmanSalary = salesmanLineParser.extractSalary(splittedLine);
            salesmanSalaries.put(salesmanName, salesmanSalary);
        }

        private void updateSalesmanTotalSalesPrice(Sales salesData) {
            String salesmanName = salesData.getSalesmanName();

            if (salesmanTotalSalesPrice.containsKey(salesmanName)) {
                salesmanTotalSalesPrice.put(salesData.getSalesmanName(), salesmanTotalSalesPrice.get(salesData.getSalesmanName())
                        .add(salesData.calculateTotalPrice()));
            } else {
                salesmanTotalSalesPrice.put(salesmanName, salesData.calculateTotalPrice());
            }
        }

        private void updateMostExpensiveSalePrice(int saleId, Sales salesData) {
            BigDecimal salesPrice = salesData.calculateTotalPrice();

            if (salesPrice.compareTo(mostExpensiveSalePrice) > 0) {
                mostExpensiveSalePrice = salesPrice;
                mostExpensiveSaleId = saleId;
            }
        }

        private Sales createSalesData(String[] splitted, int saleId) {
            String salesmanName = ""; //$NON-NLS-1$

            if (splitted.length > 4) {
                for (int i = 3; i < splitted.length; i++) {
                    salesmanName = salesmanName + splitted[i];
                }
            } else {
                salesmanName = splitted[3];
            }
            return new Sales(saleId, createSaleList(splitted[2]), salesmanName);
        }

        private Collection<Sale> createSaleList(String rawSales) {
            String[] splittedSales = rawSales.replaceAll("\\[|\\]", "").split(","); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            Collection<Sale> saleCollection = new LinkedList<Sale>();

            for (String sale : splittedSales) {
                String[] splittedSale = sale.split("-"); //$NON-NLS-1$
                SaleItem item = new SaleItem(Integer.parseInt(splittedSale[0]), new BigDecimal(splittedSale[2]));
                saleCollection.add(new Sale(item, Integer.parseInt(splittedSale[1])));
            }
            return saleCollection;
        }

        @Override
        public String getResult() {
            StringBuilder outputBuilder = new StringBuilder();

            outputBuilder.append("Amount of clients: " + amountOfClients + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
            outputBuilder.append("Amount of salesman: " + amountOfSalesman + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
            outputBuilder.append("Most expensive saleId: " + mostExpensiveSaleId + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
            outputBuilder.append("Worst salesman ever is " + calculateWorstSalesman() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
            return outputBuilder.toString();
        }

        private String calculateWorstSalesman() {
            BigDecimal worstSalesmanValue = new BigDecimal(Integer.MAX_VALUE);
            String worstSalesman = ""; //$NON-NLS-1$

            for (Entry<String, BigDecimal> salePrices : salesmanTotalSalesPrice.entrySet()) {
                BigDecimal salesmanValue = calculateSalesmanValue(salePrices.getKey(), salePrices.getValue());

                if (salesmanValue.compareTo(worstSalesmanValue) < 0) {
                    worstSalesmanValue = salesmanValue;
                    worstSalesman = salePrices.getKey();
                }
            }
            return worstSalesman;
        }

        private BigDecimal calculateSalesmanValue(String key, BigDecimal value) {
            BigDecimal salesmanSalary = salesmanSalaries.get(key);

            if (salesmanSalary != null) {
                return value.divide(salesmanSalary, RoundingMode.CEILING);
            }
            return new BigDecimal(Integer.MAX_VALUE);
        }
    }
}
