package com.ilegra.dataanalyzer.analysis;

import com.ilegra.dataanalyzer.domain.EntityType;

/**
 * Throwed when {@link EntityTypeResolver} was unable to determine an {@link EntityType}.
 *
 * @author marcio
 */
public class UnknownEntityException extends Exception {

    private static final long serialVersionUID = -5715562366428544874L;

    public UnknownEntityException(NumberFormatException e) {
        super(e);
    }

    public UnknownEntityException(String message) {
        super(message);
    }
}
