package com.ilegra.dataanalyzer.analysis.parsing;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

/**
 * Parses a line that refers to a salesman.
 *
 * @author marcio
 */
@Component
public class SalesmanLineParser {

    public String extractName(String[] splittedLine) {
        String salesmanName = ""; //$NON-NLS-1$

        if (splittedLine.length > 4) {
            for (int i = 2; i < splittedLine.length - 1; i++) {
                salesmanName = salesmanName + splittedLine[i];
            }
        } else {
            salesmanName = splittedLine[2];
        }
        return salesmanName;
    }

    public BigDecimal extractSalary(String[] splittedLine) {
        return new BigDecimal(splittedLine[splittedLine.length - 1]);
    }
}
