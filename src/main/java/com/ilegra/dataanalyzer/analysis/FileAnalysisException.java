package com.ilegra.dataanalyzer.analysis;

import java.io.IOException;

/**
 * Throwed whenever happens an error during a file analysis.
 *
 * @author marcio
 */
public class FileAnalysisException extends Exception {

    private static final long serialVersionUID = -883048685484058657L;

    public FileAnalysisException(IOException e) {
        super(e);
    }
}
