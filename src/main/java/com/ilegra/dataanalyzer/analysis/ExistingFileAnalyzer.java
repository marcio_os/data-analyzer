package com.ilegra.dataanalyzer.analysis;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Analyzes existing files during the application bootstrap.
 *
 * @author marcio
 */
@Component
public class ExistingFileAnalyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExistingFileAnalyzer.class);

    private static final String DEFAULT_INPUT_FILE_SUFFIX = ".dat"; //$NON-NLS-1$

    private final String inputDirectory;
    private final Provider<FileAnalyzer> fileAnalyzerProvider;

    @Inject
    public ExistingFileAnalyzer(Provider<FileAnalyzer> fileAnalyzerProvider, @Value("${inputDirectory}") String inputDirectory) {
        this.fileAnalyzerProvider = fileAnalyzerProvider;
        this.inputDirectory = inputDirectory;
    }

    public void analyze() {
        File directory = new File(inputDirectory);

        if (directory != null) {
            processInputDirectoryFiles(directory);
        }
    }

    private void processInputDirectoryFiles(File folder) {
        for (File fileEntry : folder.listFiles()) {
            if (fileEntry.getName().endsWith(DEFAULT_INPUT_FILE_SUFFIX)) {
                try {
                    fileAnalyzerProvider.get().analyze(fileEntry);
                } catch (FileAnalysisException e) {
                    LOGGER.error("Could not analyze file {}", fileEntry); //$NON-NLS-1$
                }
            }
        }
    }
}
