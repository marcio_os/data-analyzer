package com.ilegra.dataanalyzer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.ilegra.dataanalyzer.config.ApplicationSpringContext;

/**
 * Bootstraps the application by loading spring context and starting {@link DataAnalyzer}.
 *
 * @author marcio
 */
public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationSpringContext.class);

        try {
            context.getBean(DataAnalyzer.class).start();
        } catch (Exception e) {
            LOGGER.error("Exception throwed while running Data Analyzer.", e); //$NON-NLS-1$
            System.exit(1);
        } finally {
            if (context != null) {
                context.close();
            }
        }
        System.exit(0);
    }
}
