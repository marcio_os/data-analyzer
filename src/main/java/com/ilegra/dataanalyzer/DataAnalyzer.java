package com.ilegra.dataanalyzer;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ilegra.dataanalyzer.analysis.ExistingFileAnalyzer;
import com.ilegra.dataanalyzer.io.DirectoryMonitorException;
import com.ilegra.dataanalyzer.io.FileCreationListener;
import com.ilegra.dataanalyzer.io.FileCreationMonitor;

/**
 * DataAnalyzer Application.
 *
 * @author marcio
 */
@Component
public class DataAnalyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataAnalyzer.class);

    private final FileCreationMonitor fileCreationMonitor;
    private final FileCreationListener fileCreationListener;
    private final ExistingFileAnalyzer existingFileAnalyzer;

    @Inject
    public DataAnalyzer(
            FileCreationMonitor fileCreationMonitor,
            FileCreationListener fileCreationListener,
            ExistingFileAnalyzer existingFileAnalyzer) {
        this.fileCreationMonitor = fileCreationMonitor;
        this.fileCreationListener = fileCreationListener;
        this.existingFileAnalyzer = existingFileAnalyzer;
    }

    public void start() throws DirectoryMonitorException, InterruptedException {
        LOGGER.info("Starting Ilegra DataAnalyzer..."); //$NON-NLS-1$
        try {
            fileCreationMonitor.start(fileCreationListener);
            existingFileAnalyzer.analyze();
            sleepForever();
        } finally {
            fileCreationMonitor.stop();
            LOGGER.info("Finishing Ilegra DataAnalyzer... See you."); //$NON-NLS-1$
        }
    }

    private void sleepForever() throws InterruptedException {
        while(true) {
            Thread.sleep(Long.MAX_VALUE);
        }
    }
}
