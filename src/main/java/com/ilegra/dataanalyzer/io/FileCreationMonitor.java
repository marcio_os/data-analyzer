package com.ilegra.dataanalyzer.io;

/**
 * Monitors file creation and notifies a {@link FileCreationListener}.
 *
 * @author marcio
 */
public interface FileCreationMonitor {

    /**
     * Starts {@link FileCreationMonitor} with given {@link FileCreationListener}.
     *
     * @param listener
     *            {@link FileCreationListener} that will be notified about file creation events.
     * @throws DirectoryMonitorException
     *             Whenever happens an error during the monitoring.
     */
    void start(FileCreationListener listener) throws DirectoryMonitorException;

    /**
     * Stops {@link FileCreationMonitor}.
     */
    void stop();
}