package com.ilegra.dataanalyzer.io;

import java.io.File;

/**
 * A listener that receives events triggered by a {@link DirectoryFileCreationMonitor} when a file is created inside
 * default input directory.
 *
 * @author marcio
 */
public interface FileCreationListener {

    /**
     * {@link DirectoryFileCreationMonitor} observed that a new file was created inside default input directory.
     *
     * @param file
     *            Created {@link File}.
     */
    void onFileCreate(File file);
}