package com.ilegra.dataanalyzer.io;

import java.io.File;

import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Asynchronusly monitors default input directory and notify a {@link AnalyzerFileCreationListener} when a new file is created in it.
 *
 * @author marcio
 */
@Component
public class DirectoryFileCreationMonitor implements FileCreationMonitor {

    private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryFileCreationMonitor.class);

    private static final String INPUT_FILE_SUFFIX = ".dat"; //$NON-NLS-1$
    private static final long CHECK_INTERVAL_MILLIS = 3000L;

    private final FileAlterationObserver observer;
    private final FileAlterationListenerFactory listenerFactory;

    private volatile boolean running;

    @Inject
    public DirectoryFileCreationMonitor(
            @Value("${inputDirectory}") String inputDirectory, FileAlterationListenerFactory listenerFactory) {
        this.listenerFactory = listenerFactory;
        this.observer = new FileAlterationObserver(inputDirectory, FileFilterUtils.suffixFileFilter(INPUT_FILE_SUFFIX));
    }

    @Override
    @Async
    public void start(FileCreationListener listener) throws DirectoryMonitorException {
        try {
            LOGGER.debug("Starting monitoring input directory {}", observer.getDirectory().getAbsolutePath()); //$NON-NLS-1$
            observer.addListener(listenerFactory.createForCreationListener(listener));
            initializeObserver();
        } catch (Exception e) {
            throw new DirectoryMonitorException(e);
        }
    }

    @Override
    @PreDestroy
    public void stop() {
        try {
            LOGGER.debug("Stopping monitoring input directory {}", observer.getDirectory().getAbsolutePath()); //$NON-NLS-1$
            running = false;
            observer.destroy();
        } catch (Exception e) {
            LOGGER.error("Error while stopping monitor", e); //$NON-NLS-1$
        }
    }

    private void initializeObserver() throws Exception, InterruptedException {
        observer.initialize();
        running = true;
        while (running) {
            LOGGER.debug("Checking for file creation"); //$NON-NLS-1$
            observer.checkAndNotify();
            Thread.sleep(CHECK_INTERVAL_MILLIS);
        }
    }

    /**
     * Factory of {@link FileAlterationListener} that wraps a {@link AnalyzerFileCreationListener} to notify it about file creation events
     * only.
     *
     * @author marcio
     */
    @Component
    public static class FileAlterationListenerFactory {

        public FileAlterationListener createForCreationListener(final FileCreationListener creationListener) {

            return new FileAlterationListener() {

                @Override
                public void onStop(FileAlterationObserver observer) {
                }

                @Override
                public void onStart(FileAlterationObserver observer) {
                }

                @Override
                public void onFileDelete(File file) {
                }

                @Override
                public void onFileCreate(File file) {
                    creationListener.onFileCreate(file);
                }

                @Override
                public void onFileChange(File file) {
                    creationListener.onFileCreate(file);
                }

                @Override
                public void onDirectoryDelete(File directory) {
                }

                @Override
                public void onDirectoryCreate(File directory) {
                }

                @Override
                public void onDirectoryChange(File directory) {
                }
            };
        }
    }
}
