package com.ilegra.dataanalyzer.io;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.ilegra.dataanalyzer.analysis.FileAnalysisException;
import com.ilegra.dataanalyzer.analysis.FileAnalyzer;

/**
 * A {@link FileCreationListener} that delegates new file analysis to a {@link FileAnalyzer} upon file creation.
 *
 * @author marcio
 */
@Component
public class AnalyzerFileCreationListener implements FileCreationListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyzerFileCreationListener.class);

    private final Provider<FileAnalyzer> fileAnalyzerProvider;

    @Inject
    public AnalyzerFileCreationListener(Provider<FileAnalyzer> fileAnalyzerProvider) {
        this.fileAnalyzerProvider = fileAnalyzerProvider;
    }

    /**
     * Asynchronously gets new {@link FileAnalyzer} and delegates file analysis to it.
     *
     * @param file
     *            Created {@link File}.
     */
    @Override
    @Async
    public void onFileCreate(File file) {
        try {
            LOGGER.debug("New file created, requesting analysis."); //$NON-NLS-1$
            fileAnalyzerProvider.get().analyze(file);
        } catch (FileAnalysisException e) {
            LOGGER.error("Error while analyzing file {}", file.getName(), e); //$NON-NLS-1$
        }
    }
}
