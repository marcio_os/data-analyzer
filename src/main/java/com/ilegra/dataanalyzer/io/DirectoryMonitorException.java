package com.ilegra.dataanalyzer.io;

/**
 * Throwed when an error happens within the directory monitoring process.
 * 
 * @author marcio
 */
public class DirectoryMonitorException extends Exception {

    private static final long serialVersionUID = 1139710355274881386L;

    public DirectoryMonitorException(Throwable e) {
        super(e);
    }
}
