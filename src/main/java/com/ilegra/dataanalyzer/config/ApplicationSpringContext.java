package com.ilegra.dataanalyzer.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;

/**
 * Application Spring Context.
 *
 * @author marcio
 */
@Configuration
@ComponentScan(basePackages = { "com.ilegra.dataanalyzer" })
@Import(AsyncConfiguration.class)
public class ApplicationSpringContext {

    @Bean
    public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
        PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();

        configurer.setLocation(new ClassPathResource("/settings.properties")); //$NON-NLS-1$
        return configurer;
    }
}
